<?php

namespace App\Http\Controllers\Api;

use App\Address;
use App\District;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Transformers\AddressTransformer;
use App\Transformers\DistrictTransformer;
use App\Transformers\VdcTransformer;
use App\Transformers\WardTransformer;
use App\Transformers\ZoneTransformer;
use App\Vdc;
use App\Ward;
use App\Zone;
use Illuminate\Http\Request;

class AddressController extends ApiController
{
	public function zone(Request $request){
        return $this->respondWith(Zone::all(), new ZoneTransformer);
    }

    public function discritct(Request $request, $zone_id=null){
        return $this->respondWith(District::where('zone_id', $zone_id)->get(), new DistrictTransformer);
    }

    public function vdc(Request $request, $dis_id=null){
        return $this->respondWith(Vdc::where('district_id', $dis_id)->get(), new VdcTransformer);
    }
    
    public function ward(Request $request, $vdc_id=null){
        return $this->respondWith(Ward::where('vdc_id', $vdc_id)->get(), new WardTransformer);
    } 

    public function address(Request $request){
        return $this->respondWith(Address::all(), new AddressTransformer);
    }  
}
