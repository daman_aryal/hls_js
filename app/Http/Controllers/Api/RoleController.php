<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests;
use App\Injection\RoleForm;
use App\Role;
use App\Transformers\RoleTransformer;

use Illuminate\Http\Request;

class RoleController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->respondWith(Role::all(), new RoleTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permisson = [];

        $role = Role::create($request->get('role'));
        
        foreach ($request->get('permission') as $key => $value) {
            $permisson[] = ['id'=>$value];
        }
        
        foreach ($permisson as $key => $value) {
            $role->attachPermission($value);
        }
        /*$role->attachPermissions($request->get('permission'));
        foreach($request->get('permission') as $key => $value) {
        }*/

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->respondWith(Role::find($id), new RoleTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permisson = [];

        $role = Role::find($id);
        $role->update($request->get('role'));
        
        foreach ($request->get('permission') as $key => $value) {
            $permisson[] = ['id'=>$value];
        }
        
        foreach ($permisson as $key => $value) {
            $role->savePermissions($value);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        return $this->respondWithArray([
            'success' => true,
            'message' => 'Successfully deleted role.'
        ]);
    }
    
    public function persmission_for_model(RoleForm $roleForm){
        return response()->json([
            'user_model' => $roleForm->user_model(), 
            'role_model' => $roleForm->role_model(), 
            'dept_model' => $roleForm->dept_model(),
            'doctor_model' => $roleForm->doctor_model(),
            'patient_model' => $roleForm->patient_model()
        ]);
    }
}
