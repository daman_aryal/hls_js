<?php

namespace App\Http\Controllers\Api;

use App\District;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests;
use App\Patient;
use App\Transformers\PatientTransformer;
use App\Transformers\ZoneTransformer;
use App\Zone;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class PatientController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //return $this->respondWith(Patient::all(), new PatientTransformer);
        $patient = Patient::select(['id','fname', 'age', 'gender', 'bloodGroup','department','consultantName']);
        return Datatables::of($patient)
        ->addColumn('action', function ($patient)
            {
                $buttons = '<button type="button" id="'.$patient->id.'" class="editP btn btn-info"><i class="fa fa-edit"></i></button>
                <button type="button" id="'.$patient->id.'" class="delP btn btn-danger"><i class="fa fa-trash"></i></button>';
                return $buttons;
            })->make();
    }

    public function patient_detail_print($id){
       
        $patient = Patient::find($id);
        return view('backend.patient_print', compact('patient'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['zone'] = $request->get('zone')['id'];
        $data['district'] = $request->get('district')['id'];
        $data['vdc'] = $request->get('vdc')['id'];
        $data['ward'] = $request->get('ward')['id'];
        $patient = Patient::create($data);
        
        return response()->json([
            'p_id' => $patient->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->respondWith(Patient::find($id), new PatientTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patient = Patient::find($id);
        $patient->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::find($id);
        $patient->delete();
        return $this->respondWithArray([
            'success' => true,
            'message' => 'Successfully deleted patient.'
        ]);
    }
}
