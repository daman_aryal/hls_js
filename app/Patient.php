<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
    						'fname','mname','lname','gender',
    						'occupation','dob','age',
    						'bloodGroup','maritalStatus','zone',
    						'district','vdc','ward','country','phoneNo',
    						'mobileNo','department','consultantName','membershipId'
						];
}
