<?php 

namespace App\Injection;

use App\Permission;
use App\Role;
use Auth;
use DB;

class RoleForm{

	protected $permission;

	protected $permissionTransformer;

	public function __construct(){
		$this->permission = Permission::all();
	}

	public function user_model(){
		$user_model = $this->permission->where('catagory','user-catagory')->pluck('id','name');
		return $user_model;
	}
	
	public function role_model(){
		$role_model = $this->permission->where('catagory','role-catagory')->pluck('id','name');
		return $role_model;
	}
	
	public function dept_model(){
		$dept_model = $this->permission->where('catagory','department-catagory')->pluck('id','name');
		return $dept_model;
	}

	public function doctor_model(){
		$doctor_model = $this->permission->where('catagory','doctor-catagory')->pluck('id','name');
		return $doctor_model;
	}

	public function patient_model(){
		$patient_model = $this->permission->where('catagory','patient-catagory')->pluck('id','name');
		return $patient_model;
	}

	public function checked_permission($permission, $role_id){
		$permission = DB::table('permission_role')
						->where('role_id',$role_id)		
						->where('permission_id', $permission)
						->exists();
		if($permission){
			return true;
		}else{
			return false;
		}				
	}
}