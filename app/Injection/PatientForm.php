<?php

namespace App\Injection;

use App\Department;
use App\Doctor;
use App\Patient;

class PatientForm {
	public function doctor(){
		$doctor = Doctor::pluck('name', 'id');
		return $doctor;	
	}
	public function dept(){
		$dept = Department::pluck('name', 'id');
		return $dept;	
	}
	public function edit_doctor($doctor_id){
		$doctor = Patient::where('doctor_id',$doctor_id)
						->exists();
		if($doctor){
			return true;
		}else{
			return false;
		}		
	}	
	public function edit_dept($dept_id){
		$dept = Patient::where('department_id',$dept_id)
						->exists();
		if($dept){
			return true;
		}else{
			return false;
		}		
	}	
}