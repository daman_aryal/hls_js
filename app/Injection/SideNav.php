<?php

namespace App\Injection;
use Request;


class SideNav{
	public function active_nav($value)
	{
		if(strpos(Request::path(), $value) !== false){
			return "active";
		}

	}
}