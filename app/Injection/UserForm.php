<?php
namespace App\Injection;

use App\Role;

class UserForm{

	protected $role;

	public function __construct(){
		$this->role =  Role::get();
	}

	public function su()
	{
		$su = $this->role->where('user_type_id', 1)->pluck('display_name', 'id');
		return $su;
	}

	public function admin()
	{
		$admin = $this->role->where('user_type_id', 2)->pluck('display_name', 'id');
		return $admin;
	}

	public function libarian()
	{
		$libarian = $this->role->where('user_type_id', 3)->pluck('display_name', 'id');
		return $libarian;
	}
	
	public function student()
	{
		$student = $this->role->where('user_type_id', 4)->pluck('display_name', 'id');
		return $student;
	}

}