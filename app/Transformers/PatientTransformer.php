<?php

namespace App\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class PatientTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var  array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var  array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var  object
     * @return array
     */
    public function transform($patient)
    {
        return [
            'id' => $patient->id,
            'fname' => $patient->fname,
            'lname' => $patient->lname,
            'gender' => $patient->gender,
            'occupation' => $patient->occupation,
            'dob' => $patient->dob,
            'age' => $patient->age,
            'bloodGroup' => $patient->bloodGroup,
            'maritalStatus' => $patient->maritalStatus,
            'country' => $patient->country,
            'phoneNo' => $patient->phoneNo,
            'mobileNo' => $patient->mobileNo,
            'department' => $patient->department,
            'consultantName' => $patient->consultantName
        ];
    }

}
