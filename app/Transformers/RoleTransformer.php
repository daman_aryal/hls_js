<?php

namespace App\Transformers;

use Illuminate\Support\Facades\DB;
use League\Fractal;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var  array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var  array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var  object
     * @return array
     */
    public function transform($role)
    {
        $permission_pivot = DB::table('permission_role')->where('role_id', $role->id)->pluck('permission_id'); 
        
        return [
            'id' => $role->id,
            'name' => $role->name,
            'display_name' => $role->display_name,
            'description' => $role->description,
            'description' => $role->description,
            'permission' => $permission_pivot
        ];
    }

}
