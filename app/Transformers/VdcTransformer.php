<?php

namespace App\Transformers;

use App\Vdc;
use Hashids;
use League\Fractal;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class VdcTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var  array
     */
    protected $availableIncludes = ['vdcs'];

    /**
     * List of resources to automatically include
     *
     * @var  array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var  object
     * @return array
     */
    public function transform(Vdc $vdc)
    {
        return [
            'id' => $vdc->id,
            'vdc' => $vdc->vdc,
            'district_id' => $vdc->district_id
        ];
    }
}
