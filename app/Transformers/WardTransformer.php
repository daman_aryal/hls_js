<?php

namespace App\Transformers;
use App\Ward;
use Hashids;
use League\Fractal;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class WardTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var  array
     */
    protected $availableIncludes = ['wards'];

    /**
     * List of resources to automatically include
     *
     * @var  array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var  object
     * @return array
     */
    public function transform(Ward $ward)
    {
        return [
            'id' => $ward->id,
            'ward' => $ward->ward,
            'vdc_id' => $ward->vdc_id
        ];
    }
}
