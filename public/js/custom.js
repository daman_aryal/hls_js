//single type ahedad
var singleTypeHead = function(url, inputId, field){
    singleUrl = url+'?'+field+'=%QUERY';
    typeAhead(singleUrl,inputId, field);
}



var typeAhead = function(url, inputId, field){
    var engine = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace(field),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote:{
                url: url,
                wildcard: '%QUERY'
            }
    });
    $(inputId).typeahead({
        hint: true,
        highlight: true,
        minLength: 0
    }, {
        source: engine.ttAdapter(),
        // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
        name: field,
        // the key from the array we want to display (name,id,email,etc...)
        displayKey:field,
        templates: {
                empty: [
                    '<div class="empty-message">unable to find any</div>'
                ],
                header: [
                    '<div class="list-group search-results-dropdown">'
                ],
                suggestion: function (data) {
                    return '<h5 class="media-heading" v-value="'+data.id+'">"'+data[field]+'"</h6>'
                }
          }
    });   
}