<?php

use App\Department;
use App\Doctor;
use Illuminate\Database\Seeder;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $doctors = [
        	[
        		'name' => 'Dr. Bhola Subedi',
        		'code' =>'d1',
        	],
        	[
        		'name' => 'Dr. Rajan Rijal',
        		'code' =>'d2',
        	],
        	[
        		'name' => 'Dr. Dipa Neupane',
        		'code' =>'d3',
        	],
        	[
        		'name' => 'Dr. Rajan Mahat',
        		'code' =>'c4',
        	],
        	[
        		'name' => 'Dr. Mohan Kc',
        		'code' =>'d5',
        	]
        ];

        $department = new Department();
        $department->pluck('id')->toArray();
        $dept_rand = count($department);
        
        foreach ($doctors as $key => $value) {
            $doctor_mysql = new Doctor();
            $doctor_mysql->delete();

            $doctor_mysql->name = $value['name'];
            $doctor_mysql->code = $value['code'];
             
            $doctor_mysql->save();
            /*$doctor_mysql->dept_pivot()->attach([rand(1, $dept_rand)]);   */
        }
    }
}
