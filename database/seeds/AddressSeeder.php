<?php

use App\Address;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('address')->delete();
        $address = [
        	[
                'id' => 1,
        		'address' => 'Samakhusi'
        	],
        	[
                'id' => 2,
        		'address' => 'Gongabu'
        	],

        	[
                'id' => 3,
        		'address' => 'Tinkune'
        	]
        ];
		
		foreach ($address as $key => $value) {
            Address::create($value);
        }
    }
}
