<?php

use App\Zone;
use Illuminate\Database\Seeder;

class ZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zones')->delete();
        $zone = [
        	[
                'id' => 1,
        		'zone' => 'Bagmati'
        	],
        	[
                'id' => 2,
        		'zone' => 'Gandaki'
        	]
        ];
		
		foreach ($zone as $key => $value) {
            Zone::create($value);
        }
    }
}
