<?php


use App\District;
use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->delete();
        $district = [
        	[
        		'id' => 1,
        		'district' => 'Kathmandu',
        		'zone_id' => 1
        	],
        	[
        		'id' => 2,
        		'district' => 'Lalitpur',
        		'zone_id' => 1
        	],
        	[
        		'id' => 3,
        		'district' => 'Bhaktapur',
        		'zone_id' => 1
        	],
            [
                'id' => 4,
                'district' => 'Gorkha',
                'zone_id' => 2
            ],

            [
                'id' => 5,
                'district' => 'Tanahu',
                'zone_id' => 2
            ],
            [
                'id' => 6,
                'district' => 'Lamjung',
                'zone_id' => 2
            ]

        ];
		
		foreach ($district as $key => $value) {
            District::create($value);
        }
    }
}
