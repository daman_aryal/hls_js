<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionSeeder::class);
        $this->call(SuSeeder::class);
        $this->call(DeptSeeder::class);
        $this->call(DoctorSeeder::class);
        $this->call(ZoneSeeder::class);
        $this->call(DistrictSeeder::class);
        $this->call(VdcSeeder::class);
        $this->call(WardSeeder::class);
        $this->call(AddressSeeder::class);
    }
}
