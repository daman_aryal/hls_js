<?php

use App\Vdc;
use Illuminate\Database\Seeder;

class VdcSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vdcs')->delete();
        $vdc = [
        	[
        		'id' => 1,
        		'vdc' => 'Ktm_vdc_1',
        		'district_id' => 1
        	],
        	[
        		'id' => 2,
        		'vdc' => 'Ktm_vdc_2',
        		'district_id' => 1
        	],
        	[
        		'id' => 3,
        		'vdc' => 'Ktm_vdc_3',
        		'district_id' => 1
        	],
            [
                'id' => 4,
                'vdc' => 'grk_vdc_1',
                'district_id' => 4
            ],
            [
                'id' => 5,
                'vdc' => 'grk_vdc_2',
                'district_id' => 4
            ],
            [
                'id' => 6,
                'vdc' => 'bkt_vdc_1',
                'district_id' => 3
            ]
        ];
		
		foreach ($vdc as $key => $value) {
            Vdc::create($value);
        }
    }
}
