<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$permission = [
        	[
                'name' => 'user-create',
        		'display_name' => 'Create User',
        		'description' => 'can create user',
                'catagory' => 'user-catagory'
        	],
        	[
        		'name' => 'user-edit',
        		'display_name' => 'Edit User',
        		'description' => 'can edit the user',
                'catagory' => 'user-catagory'
        	],
        	[
        		'name' => 'user-delete',
        		'display_name' => 'Delete user',
        		'description' => 'can delete the user',
                'catagory' => 'user-catagory'
        	],
            [
                'name' => 'user-list',
                'display_name' => 'Show the user list',
                'description' => 'can see the user list',
                'catagory' => 'user-catagory'
            ],
            /*role define*/
            [
                'name' => 'role-create',
                'display_name' => 'Create role',
                'description' => 'can create role',
                'catagory' => 'role-catagory'
            ],
            [
                'name' => 'role-edit',
                'display_name' => 'Edit role',
                'description' => 'can edit the role',
                'catagory' => 'role-catagory'
            ],
            [
                'name' => 'role-delete',
                'display_name' => 'Delete role',
                'description' => 'can delete the role',
                'catagory' => 'role-catagory'
            ],
            [
                'name' => 'role-list',
                'display_name' => 'Show the user role',
                'description' => 'can see the user role',
                'catagory' => 'role-catagory'
            ],
            /*department permission*/
            [
                'name' => 'department-create',
                'display_name' => 'Department stock',
                'description' => 'can create department',
                'catagory' => 'department-catagory'
            ],
            [
                'name' => 'department-edit',
                'display_name' => 'Edit Department',
                'description' => 'can edit the department',
                'catagory' => 'department-catagory'
            ],
            [
                'name' => 'department-delete',
                'display_name' => 'Delete department',
                'description' => 'can delete the department',
                'catagory' => 'department-catagory'
            ],
            [
                'name' => 'department-list',
                'display_name' => 'Show the department list',
                'description' => 'can see the department list',
                'catagory' => 'department-catagory'
            ],
            /*doctor rule define*/
            [
                'name' => 'doctor-create',
                'display_name' => 'Create doctor',
                'description' => 'can create doctor',
                'catagory' => 'doctor-catagory'
            ],
            [
                'name' => 'doctor-edit',
                'display_name' => 'Edit doctor',
                'description' => 'can edit the doctor',
                'catagory' => 'request-catagory'
            ],
            [
                'name' => 'doctor-delete',
                'display_name' => 'Delete doctor',
                'description' => 'Can delete the doctor',
                'catagory' => 'doctor-catagory'
            ],
            [
                'name' => 'doctor-list',
                'display_name' => 'Show the doctor',
                'description' => 'Can view the doctor',
                'catagory' => 'doctor-catagory'
            ],
            /*patient rule define*/
            [
                'name' => 'patient-create',
                'display_name' => 'Create patient',
                'description' => 'can create patient',
                'catagory' => 'patient-catagory'
            ],
            [
                'name' => 'patient-edit',
                'display_name' => 'Edit patient',
                'description' => 'can edit the patient',
                'catagory' => 'patient-catagory'
            ],
            [
                'name' => 'patient-delete',
                'display_name' => 'Delete patient',
                'description' => 'can delete the patient',
                'catagory' => 'patient-catagory'
            ],
            [
                'name' => 'patient-list',
                'display_name' => 'Show the patient',
                'description' => 'can see the patient',
                'catagory' => 'patient-catagory'
            ],
        ];

        
		/**
		 * [BadMethodCallException] 
		 * This cache store does not support tagging
		 * @https://github.com/Zizaco/entrust/issues/468
		**/                 
  		
        DB::table('permissions')->delete();
        foreach ($permission as $key => $value) {
           Permission::create($value);
        }
    }
}
