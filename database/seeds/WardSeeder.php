<?php

use App\Ward;
use Illuminate\Database\Seeder;

class WardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wards')->delete();
        $vdc = [
        	[
        		'id' => 1,
        		'ward' => 'ward_1_vdc_1',
        		'vdc_id' => 1
        	],
        	[
        		'id' => 2,
        		'ward' => 'ward_2_vdc_1',
        		'vdc_id' => 1
        	],
        	[
        		'id' => 3,
        		'ward' => 'ward_1_vdc_4',
        		'vdc_id' => 4
        	],
        	[
        		'id' => 4,
        		'ward' => 'ward_1_vdc_6',
        		'vdc_id' => 6
        	],
        	[
        		'id' => 5,
        		'ward' => 'ward_3_vdc_1',
        		'vdc_id' => 1
        	],
        	[
        		'id' => 6.1,
        		'ward' => 'ward_4_vdc_1',
        		'vdc_id' => 1
        	]
        ];
		
		foreach ($vdc as $key => $value) {
            Ward::create($value);
        }
    }
}
