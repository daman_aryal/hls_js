<?php

use App\Department;
use Illuminate\Database\Seeder;

class DeptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $department = [
        	[
        		'name' => 'Cardiology',
        		'code' =>'c1',
        	],
        	[
        		'name' => 'Gynecology',
        		'code' =>'c2',
        	],
        	[
        		'name' => 'Neurology',
        		'code' =>'c3',
        	],
        	[
        		'name' => 'Orthopaedics',
        		'code' =>'c4',
        	],
        	[
        		'name' => 'Radiology',
        		'code' =>'c5',
        	]
        ];

       
        foreach ($department as $key => $value) {
            $department = new Department();
            $department->delete();
            $department->name = $value['name'];
            $department->code = $value['code'];
            $department->save();
        }
    }
}
