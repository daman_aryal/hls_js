<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('mname');
            $table->string('fname');
            $table->string('lname');
            $table->string('gender');
            $table->string('occupation')->nullable();
            $table->date('dob')->nullable();
            $table->integer('age');
            $table->string('bloodGroup')->nullable();
            $table->string('maritalStatus')->nullable();
            $table->integer('zone')->nullable();
            $table->integer('district')->nullable();
            $table->integer('vdc')->nullable();
            $table->integer('ward')->nullable();
            $table->string('country')->nullable();
            $table->integer('phoneNo')->nullable();
            $table->integer('mobileNo')->nullable();
            $table->string('department');
            $table->string('consultantName');
            $table->integer('membershipId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
