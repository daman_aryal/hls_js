<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /->box-header -->
        <div class="box-body table-responsive no-padding">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><strong>{{$patient->fname + $patient->lname }}</strong></h4>
                </div>
                <div class="panel-body">
                    <ul style="list-style:none">
                        <li>Gender : <strong>{{$patient->gender}}</strong></li>
                        <li>Occupation : <strong>{{$patient->occupation}}</strong></li>
                        <li>Date of Birth : <strong>{{$patient->dob}}</strong></li>
                        <li>Age : <strong>{{$patient->age}}</strong></li>
                        <li>Blood Group : <strong>{{$patient->bloodGroup}}</strong></li>
                        <li>Maritual Status : <strong>{{$patient->maritalStatus}}</strong></li>
     
                        <li>Country : <strong>{{$patient->country}}</strong></li>
                        <li>Phone No : <strong>{{$patient->phoneNo}}</strong></li>
                        <li>Mobile No : <strong>{{$patient->mobileNo}}</strong></li>
                        <li>Department : <strong>{{$patient->department}}</strong></li>
                        <li>Consultant Name : <strong>{{$patient->consultantName}}</strong></li>
                        <li>Membership id : <strong>{{$patient->membershipId}}</strong></li>
                    </ul>
                </div>
            </div>  
        </div>
        <!-- /->box-body -->
      </div>
      <!-- /->box -->
    </div>
  </div>
</section>