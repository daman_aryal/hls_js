@extends('admin.master')
@section('contents')
<div class="login">
    <div class="logo">
        <a href="">
            <img src="/img/mavorion.png" style="height: 30px;" alt="" /> </a>
    </div>
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
            <div class="form-title">
                <span class="form-title">Welcome.</span>
                <span class="form-subtitle">Please login.</span>
            </div>
            <div class="alert alert-danger" hidden="true">
                <button class="close" data-close="alert"></button>
                <span> Enter any email and password. </span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
            <div class="form-group">
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
            <div class="form-actions">
                <button type="submit" class="btn red btn-block uppercase">Login</button>
            </div>
            <div class="form-actions">
                <div class="pull-left">
                    <label class="rememberme check">
                        <input type="checkbox" name="remember" value="1" />Remember me </label>
                </div>
                <div class="pull-right forget-password-block">
                    <a href="{{ url('/password/reset') }}" id="forget-password" class="forget-password">Forgot Password?</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
