@extends('master')
@section('title','Patient | Registration')
@section('headercss')
{{-- section for distinct css... --}}
  <link href="{{ url('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('css/toastr.min.css') }}" rel="stylesheet">

@endsection
@section('body')

    <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
        <div class="page-content" id="manage-vue">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <button type="button" id="create" class="btn btn-success" data-toggle="modal" data-target="#create-item">
                                <span class="caption-subject bold uppercase">Create</span>
                            </button>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Doc Code</th>
                                    <th>Name</th>
                                    <th width="200px">Qualification</th>
                                    <th>Specialization</th>
                                    <th>Action</th>
                                </tr>
                                <tr v-for="item in itemss">
                                <td>@{{ item.docCode }}</td>
                                    <td>@{{ item.name }}</td>
                                    <td>@{{ item.qualification }}</td>
                                    <td>@{{ item.specialization }}</td>
                                    <td>    
                                      <button class="btn btn-primary" @click.prevent="editItem(item)">Edit</button>
                                      <button class="btn btn-danger" @click.prevent="deleteItem(item)">Delete</button>
                                    </td>
                                </tr>
                            </table>
                            <nav>
                                <ul class="pagination">
                                    <li v-if="pagination.current_page > 1">
                                        <a href="#" aria-label="Previous"
                                           @click.prevent="changePage(pagination.current_page - 1)">
                                            <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li v-for="page in pagesNumber"
                                        v-bind:class="[ page == isActived ? 'active' : '']">
                                        <a href="#"
                                           @click.prevent="changePage(page)">@{{ page }}</a>
                                    </li>
                                    <li v-if="pagination.current_page < pagination.last_page">
                                        <a href="#" aria-label="Next"
                                           @click.prevent="changePage(pagination.current_page + 1)">
                                            <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- Create Item Modal -->
        <div class="modal fade" id="create-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Item</h4>
              </div>
              <div class="modal-body">

                    <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="createItem">

                   <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" id="name" name="name" class="form-control" v-model="newItem.name" />
                        <label for="name">Name:</label>
                        <span v-if="formErrors['name']" class="error text-danger help-block">@{{ formErrors['name'] }}</span>
                    </div>
                   <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" id="docCode"   name="docCode" class="form-control" v-model="newItem.docCode" />
                        <label for="docCode">Doctor Code</label>
                        <span v-if="formErrors['docCode']" class="error text-danger help-block">@{{ formErrors['docCode'] }}</span>
                    </div>
                   <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" id="qualification" name="qualification" class="form-control" v-model="newItem.qualification" />
                        <label for="qualification">Qualification</label>
                        <span v-if="formErrors['qualification']" class="error text-danger help-block">@{{ formErrors['qualification'] }}</span>
                    </div>
                   <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" name="specialization" id="specialization" class="form-control" v-model="newItem.specialization" />
                        <label for="specialization">Specialization:</label>
                        <span v-if="formErrors['specialization']" class="error text-danger help-block">@{{ formErrors['specialization'] }}</span>
                    </div>
                   <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>

                    </form>

                
              </div>
            </div>
          </div>
        </div>

        <!-- Edit Item Modal -->
        <div class="modal fade" id="edit-s" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
              </div>
              <div class="modal-body">

                    <form method="POST" enctype="multipart/form-data" v-on:submit.prevent="updateItem(fillItem.id)">

                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" id="docCode" name="docCode" class="form-control" v-model="fillItem.docCode" />
                        <label for="docCode">Doctor Code:</label>
                        <span v-if="formErrorsUpdate['docCode']" class="error text-danger">@{{ formErrorsUpdate['docCode'] }}</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" id="name" name="name" class="form-control" v-model="fillItem.name" />
                        <label for="name">Full Name:</label>
                        <span v-if="formErrorsUpdate['name']" class="error text-danger">@{{ formErrorsUpdate['name'] }}</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" id="qualification" name="qualification" class="form-control" v-model="fillItem.qualification" />
                        <label for="qualification">Qualification</label>
                        <span v-if="formErrorsUpdate['qualification']" class="error text-danger">@{{ formErrorsUpdate['qualification'] }}</span>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input type="text" id="specialization" name="specialization" class="form-control" v-model="fillItem.specialization" />
                        <label for="specialization">Specialization</label>
                        <span v-if="formErrorsUpdate['specialization']" class="error text-danger">@{{ formErrorsUpdate['specialization'] }}</span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>

                    </form>

              </div>
            </div>
          </div>
        </div>
        </div>
    </div>

@endsection
@section('footerscript')

<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ url('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/pages/scripts/table-datatables-buttons.min.js') }}" type="text/javascript"></script>
{{-- section for distinct script --}}

    <script type="text/javascript" src="{{ url('js/toastr.min.js') }}"></script>

    {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script> --}}
    <script type="text/javascript" src="{{ url('js/vue.min.js') }}"></script>

    <script type="text/javascript" src="{{ url('js/vue-resource.min.js') }}"></script>
    <script type="text/javascript" src="{{url('js/doctor.js')}}"></script>
    <script type="text/javascript" src="{{url('js/custom.js')}}"></script>

@endsection