<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta id="token" name="token" value="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />


        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ url('assets/plugins/datatables/media/css/dataTables.bootstrap.css') }}">
        <link href="{{ url('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />



        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->



        <!-- BEGIN THEME GLOBAL STYLES -->
        {{-- <link href="{{ url('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" /> --}}
        <link href="{{ url('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->


        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ url('assets/layouts/layout2/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/layouts/layout2/css/themes/grey.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        {{-- <link href="{{ url('assets/layouts/layout2/css/custom.min.css') }}" rel="stylesheet" type="text/css" /> --}}
        

        <!-- login and register styles -->
        <link href="{{ url('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/pages/css/login-2.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.css">
        <link rel="stylesheet" href="{{ url('css/pnotify.buttons.css') }}">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/4.0.5/sweetalert2.css">
        <!-- END THEME LAYOUT STYLES -->

        {{-- AdminLTE styles --}}
        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="stylesheet" href="{{ url('/static/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ url('/static/css/skin-blue.min.css') }}">
        <link rel="stylesheet" href="{{ url('/static/js/plugins/pace/pace.min.css') }}">

        {{-- custom css --}}
        <link rel="stylesheet" href="{{ url('css/custom.css') }}">   
    </head>
    <!-- END HEAD -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        @yield('contents')
        {{-- @include('admin.footer') --}}


        <!-- BEGIN CORE PLUGINS -->
        <script>
            {{--TODO: Remove else when check is done with middleware--}}
            {{--   @if(Auth::user())
                const User = {!! Fractal::includes(['role'])->item(Auth::user(), new \App\Transformers\UserTransformer)->getJson() !!};
                User.isAdmin = {{ Auth::user()->isAdmin() }};
            @else
                const User = {isAdmin: false}
            @endif --}}
        </script>
        <script src="{{ url('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->



        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ url('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/ammap/ammap.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/amcharts/amstockcharts/amstock.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->


        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ url('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->


        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ url('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->


        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ url('assets/layouts/layout2/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/layouts/layout2/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script><script src="/lte/js/bootstrap.min.js"></script>



        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ url('/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ url('/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ url('/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ url('/assets/pages/scripts/login.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->


        {{-- pnotify --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.js"></script>
        <script src="/js/pnotify.buttons.js"></script>
        {{-- Sweetalert --}}
        <script src="https://cdn.jsdelivr.net/sweetalert2/4.0.5/sweetalert2.min.js"></script>
        <script src="{{ url('static/js/plugins/datatables/jquery.dataTables.js') }}"></script>
        <script src="{{ url('assets/plugins/jprint/jQuery.print.js') }}" type="text/javascript"></script><script src="{{ url('/lte/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('js/hotkeys-master/jquery.hotkeys.js') }}"></script>
        @if(Auth::user())
        <script src="{{'/js/main.js'}}"></script>
        @endif

        <!-- Data table styles -->
        <style>
          .paginate_button, .ellipsis{
              border : 1px solid #ddd;
              padding: 7px 15px;
              margin-left: -1px;
              color: #777;
              cursor : pointer;
          }
          .current{
            background-color:#337ab7;
              color :white ;  
              border : 1px solid #337ab6; 
          }
          .dataTables_length{
            width: 50%;
            float: left;
          }
          .dataTables_filter{
            float:right;
            width: 50%;
          }
          input[type=search]{
                width: 100%;
                height: 34px;
                padding: 6px 12px;
                background-color: #fff;
                border: 1px solid #c2cad8;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
          }
          div.dataTables_wrapper div.dataTables_length select{
                height: 34px;
                padding: 6px 12px;
                background-color: #fff;
                border: 1px solid #c2cad8;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
          }

          div.dataTables_paginate{
            margin-bottom : 20px!important;
          }
         
      </style>
    </body>

</html>