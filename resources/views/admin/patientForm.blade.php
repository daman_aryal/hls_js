@extends('master')
@section('title','Patient | Registration')
@section('headercss')
{{-- section for distinct css... --}}
<link rel="stylesheet" href="{{ url('css/dropify.min.css') }}">
@endsection
@section('body')
    <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
         <div class="page-content">
             <div class="row">
                <div class="col-md-12 ">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
{{--                             <div class="caption font-green">
                                <i class="icon-note font-green"></i>
                                <span class="caption-subject bold uppercase"> Patient Registration Form</span>
                            </div> --}}
                        </div>
                        <div class="portlet-body form">
                            <form>
                                <div class="form-body">
                                    {{-- <div class="form-group text-center">
                                        <label for="patientId">Patient ID</label>
                                        <input type="text" class="form-control col-md-2" name="patientId" id="patientId" >
                                    </div> --}}
                                    <div class="col-md-7 col-xs-12" >
                                    <div class="col-md-12" style="border: 1px solid #cfcfcf;">
                                        <div class="caption font-green" style="background: #fff;margin-top: -17px;    width: 193px;font-size: 1.6em;" >
                                            <span class="caption-subject bold uppercase">Patient Details</span>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 col-xs-12">
                                                <div class="form-group form-md-line-input form-md-floating-label">
                                                    <input type="text" name="fName" class="form-control" id="fName">
                                                    <label for="fName">First Name</label>
                                                </div>
                                                <div class="form-group form-md-line-input form-md-floating-label">
                                                    <input type="text" name="lName" class="form-control" id="lName">
                                                    <label for="lName">Last Name</label>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-xs-12">
                                                <input type="file" name="patientImage" class="dropify" data-height="225" />
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        
                                        <div class="form-group form-md-line-input form-md-floating-label col-md-5">
                                            <select class="form-control edited" name="gender" id="gender">
                                                <option value="">Select Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="other">Other</option>
                                            </select>
                                            <label for="gender">Gender</label>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                                            <input type="text" class="form-control" name="occupation" id="occupation">
                                            <label for="occupation">Occupation</label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                                            <input type="text" class="form-control" name="dob" id="dob">
                                            <label for="dob">Date of Birth</label>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="form-group form-md-line-input form-md-floating-label col-md-5">
                                            <input type="text" class="form-control" name="age" id="age">
                                            <label for="age">Age</label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group form-md-line-input form-md-floating-label col-md-4" >
                                            <select class="form-control edited" name="bloodGroup" id="bloodGroup">
                                                <option value="">Select Blood Group</option>
                                                <option value="A">A + </option>
                                                <option value="A">A -</option>
                                                <option value="O">O +</option>
                                                <option value="O">O -</option>
                                            </select>
                                            <label for="bloodGroup">Blood Group</label>
                                        </div>
                                        <div class="col-md-2"></div>
                                        <div class="form-group form-md-line-input form-md-floating-label col-md-6">
                                            <select class="form-control edited" name="maritalStatus" id="maritalStatus">
                                                <option value="">Marital Status</option>
                                                <option value="married">Married</option>
                                                <option value="single">Single</option>
                                                <option value="other">Other</option>

                                            </select>
                                            <label for="maritalStatus">Marital Status</label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="md-checkbox col-md-6 margin-bottom-20">
                                            <input type="checkbox" id="mId" class="md-check">
                                            <label for="mId">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box" style="height:20px;width:20px;"></span> Membership ID:</label>
                                            <div class="form-group form-md-line-input form-md-floating-label" id="memberId">
                                            <input type="text" class="form-control" name="membershipId" id="membershipId">
                                            <label for="membershipId">Membership ID </label>
                                            </div>    
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 32px;">
                                        <div class="form-actions noborder">
                                    <button type="button" class="btn blue">Submit</button>
                                    <button type="button" class="btn default">Cancel</button>
                                </div>
                                    </div>


                                        
                                        
                                        <div class="clearfix"></div>
                                        
                                    </div>
                                    {{-- <div class="col-md-1"></div> --}}
                                    <div class="col-md-5 col-xs-12" >
                                        <div class="row" style="margin-left: 0px;">
                                            <div class="col-md-12" style="border: 1px solid #cfcfcf;">
                                                <div class="caption font-green" style="background: #fff;margin-top: -17px;    width: 204px;font-size: 1.6em;">
                                                    <span class="caption-subject bold uppercase">Contact Details</span>
                                                </div>
                                                
                                                <div class="form-group form-md-line-input form-md-floating-label">
                                                    <input type="text" class="form-control" name="address" id="address">
                                                    <label for="address">Address</label>
                                                </div>
                                                <div class="form-group form-md-line-input form-md-floating-label">
                                                    <input type="text" class="form-control" name="city" id="city">
                                                    <label for="city">City</label>
                                                </div>
                                                 <div class="form-group form-md-line-input form-md-floating-label">
                                                    <select class="form-control edited" name="country" id="country">
                                                        <option value="" selected>Country</option>
                                                        <option value="A">Nepal</option>
                                                        <option value="A">US</option>
                                                        <option value="O">Austrila</option>
                                                        <option value="O">Canada</option>
                                                    </select>
                                                    <label for="country">Country</label>
                                                </div>
                                                {{--  <div class="form-group form-md-line-input form-md-floating-label">
                                                    <input type="text" class="form-control" name="pin" id="pin">
                                                    <label for="pin">PIN</label>
                                                </div> --}}
                                                <div class="form-group form-md-line-input form-md-floating-label">
                                                    <input type="text" class="form-control" name="pnumber" id="pnumber">
                                                    <label for="pnumber">Phone Number</label>
                                                </div>
                                                <div class="form-group form-md-line-input form-md-floating-label">
                                                    <input type="text" class="form-control" name="mnumber" id="mnumber">
                                                    <label for="mnumber">Mobile Number</label>
                                                </div>
                                            <div class="clearfix"></div>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12" style="margin-top: 20px; border: 1px solid #cfcfcf;">
                                                <div class="caption font-green" style="background: #fff;margin-top: -17px;    width: 210px;font-size: 1.6em;">
                                                    <span class="caption-subject bold uppercase">Hospital Details</span>
                                                </div>
                                                <div class="form-group form-md-line-input form-md-floating-label">
                                                    <select class="form-control edited" name="department" id="department">
                                                        <option value="">Select Department</option>
                                                        <option value="Department 1">Department 1</option>
                                                        <option value="Department 2">Department 2</option>
                                                        <option value="Department 3">Department 3</option>
                                                        <option value="Department 4">Department 4</option>
                                                    </select>
                                                    <label for="department">Department</label>
                                                </div>
                                                <div class="form-group form-md-line-input form-md-floating-label">
                                                    <input type="text" class="form-control" name="consultant" id="consultant">
                                                    <label for="consultant">Consultant Name</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('footerscript')
{{-- section for distinct script --}}
<script src="{{ url('/js/dropify.min.js') }}"></script>
<script>
jQuery(document).ready(function($) {
    $('.dropify').dropify();
   // STOCK OPTIONS
    $('#mId').change(function(){
        if ($(this).is(':checked'))
            $('#memberId').show();
        else
            $('#memberId').hide();
        }).change();
});

</script>
@endsection