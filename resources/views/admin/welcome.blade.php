@extends('master')
@section('title','Hospitals | Dashboard')
@section('headercss')
{{-- section for distinct css... --}}
@endsection
@section('body')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- <h3 class="page-title"> Dashboard
                <small>dashboard & statistics</small>
            </h3> -->
            <div class="page-bar" style="background:none !important;">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Dashboard</span>
                    </li>
                </ul>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->
            <div class="row widget-row no-space margin-bottom-20">
            <div class="col-md-3 col-sm-6 col-xs-12 no-space">
                <!-- BEGIN WIDGET SOCIALS -->
                <div class="widget-socials widget-bg-color-gray">
                    <a href="{{ url('/patient-register') }}"><h2 class="widget-socials-title widget-title-color-white text-uppercase">Patient
                        <br> Registration</h2></a>
                    <div class="margin-bottom-20">
                        <strong class="widget-socials-paragraph text-uppercase">Report</strong>
                        <a class="widget-socials-paragraph" href="#">Patient Report</a>
                        <a class="widget-socials-paragraph" href="#">Analytical Report</a>
                    </div>
                    <strong class="widget-socials-paragraph text-uppercase">Supports</strong>
                </div>
                <!-- END WIDGET SOCIALS -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 no-space">
                <!-- BEGIN WIDGET SOCIALS -->
                <div class="widget-socials widget-gradient" style="background: url(../assets/layouts/layout2/img/03.jpg)"></div>
                <!-- END WIDGET SOCIALS -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 no-space">
                <!-- BEGIN WIDGET SOCIALS -->
                <div class="widget-socials widget-bg-color-fb">
                    <i class="widget-social-icon-fb icon-social-facebook"></i>
                    <h3 class="widget-social-subtitle">
                        <a href="#">Follow us
                            <br> on Facebook</a>
                    </h3>
                </div>
                <!-- END WIDGET SOCIALS -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 no-space">
                <!-- BEGIN WIDGET SOCIALS -->
                <div class="widget-socials widget-bg-color-tw">
                    <i class="widget-social-icon-tw icon-social-twitter"></i>
                    <h3 class="widget-social-subtitle">
                        <a href="#">Follow us
                            <br> on Twitter</a>
                    </h3>
                </div>
                <!-- END WIDGET SOCIALS -->
            </div>
        </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat blue">
                        <div class="visual">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="1349">0</span>
                            </div>
                            <div class="desc"> New Feedbacks </div>
                        </div>
                        <a class="more" href="javascript:;"> View more
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat red">
                        <div class="visual">
                            <i class="fa fa-bar-chart-o"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="12,5">0</span>M$ </div>
                            <div class="desc"> Total Profit </div>
                        </div>
                        <a class="more" href="javascript:;"> View more
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat green">
                        <div class="visual">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                <span data-counter="counterup" data-value="549">0</span>
                            </div>
                            <div class="desc"> New Orders </div>
                        </div>
                        <a class="more" href="javascript:;"> View more
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat purple">
                        <div class="visual">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="details">
                            <div class="number"> +
                                <span data-counter="counterup" data-value="89"></span>% </div>
                            <div class="desc"> Brand Popularity </div>
                        </div>
                        <a class="more" href="javascript:;"> View more
                            <i class="m-icon-swapright m-icon-white"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-bar-chart font-green"></i>
                                <span class="caption-subject font-green bold uppercase">Site Visits</span>
                                <span class="caption-helper">weekly stats...</span>
                            </div>
                            <div class="actions">
                                <div class="btn-group btn-group-devided" data-toggle="buttons">
                                    <label class="btn red btn-outline btn-circle btn-sm active">
                                        <input type="radio" name="options" class="toggle" id="option1">New</label>
                                    <label class="btn red btn-outline btn-circle btn-sm">
                                        <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div id="site_statistics_loading">
                                <img src="../assets/global/img/loading.gif" alt="loading" /> </div>
                            <div id="site_statistics_content" class="display-none">
                                <div id="site_statistics" class="chart"> </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
                <div class="col-md-6 col-sm-6">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-red-sunglo hide"></i>
                                <span class="caption-subject font-red-sunglo bold uppercase">Revenue</span>
                                <span class="caption-helper">monthly stats...</span>
                            </div>
                            <div class="actions">
                                <div class="btn-group">
                                    <a href="" class="btn dark btn-outline btn-circle btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Filter Range
                                        <span class="fa fa-angle-down"> </span>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="javascript:;"> Q1 2014
                                                <span class="label label-sm label-default"> past </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Q2 2014
                                                <span class="label label-sm label-default"> past </span>
                                            </a>
                                        </li>
                                        <li class="active">
                                            <a href="javascript:;"> Q3 2014
                                                <span class="label label-sm label-success"> current </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Q4 2014
                                                <span class="label label-sm label-warning"> upcoming </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div id="site_activities_loading">
                                <img src="../assets/global/img/loading.gif" alt="loading" /> </div>
                            <div id="site_activities_content" class="display-none">
                                <div id="site_activities" style="height: 228px;"> </div>
                            </div>
                            <div style="margin: 20px 0 10px 30px">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                        <span class="label label-sm label-success"> Revenue: </span>
                                        <h3>$13,234</h3>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                        <span class="label label-sm label-info"> Tax: </span>
                                        <h3>$134,900</h3>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                        <span class="label label-sm label-danger"> Shipment: </span>
                                        <h3>$1,134</h3>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6 text-stat">
                                        <span class="label label-sm label-warning"> Orders: </span>
                                        <h3>235090</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection
@section('footerscript')
{{-- section for distinct script --}}
@endsection