@extends('master')
@section('title','INVOICE')
@section('headercss')
{{-- section for distinct css... --}}
<style>
    .invoice-main{
        border: 1px  solid #cfcfcf;
        background-color: #fff;
    }
    .invoice-main .mb50{
            margin-bottom: 100px;
    }
    .invoice-main img{
        margin-top: 50px;
        margin-left: 30px;
    }
    .invoice-main h2,h5{
        margin-top: 0px;
    }
    .logo{
         padding: 45px;
    }
    .mrg30{
        padding-left: 43px;
        margin-top: 20px;
    }
    .mg10{
        margin-top: 8px;
         margin-left: 0px;
    }
    .mgl20{
      margin-top: 8px;
         margin-left: 20px;   
    }
    .mgl20 table{
    border-bottom: 1px solid #000000;
    border-top: 1px solid rgba(207, 207, 207, 0.33);
    }
    .mgl20 table thead{
        border-bottom: 1px solid #cfcfcf;
    }
</style>
@endsection
@section('body')
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="invoice-main">
                <div class="row mb50">
                    <div class="col-md-12 ">
                        <div class="col-md-4">
                            <img src="{{ url('img/barcode.gif') }}" alt="" width="200" height="50" >   
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h4>Some Slogan here</h4>
                                </div>
                                <div class="col-md-12 text-center">
                                    <h2>Name of Organization</h2>
                                </div>
                                <div class="col-md-12 text-center">
                                    <h5>Address goes here</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="logo pull-right">
                                logo Goes here
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mrg30">
                        <div class="row mg10">        
                            <div class="col-md-3">
                                <span>PATIENT ID :</span><strong><span>424224</span></strong>
                            </div>  
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-3">
                                <span class="text-center">INVOICE</span>
                            </div>
                           <div class="col-md-3">
                           </div> 
                        </div>
                        <div class="row mg10">
                            <div class="col-md-4">
                               <span>NAME   : RABI GHIMIRE </span>
                            </div> 
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <span>INVOICE NO: CS232-232345</span>
                            </div>    
                        </div>
                        <div class="row mg10">
                            <div class="col-md-4">
                                <span>AGE / GENDER   : 22 Y/M </span>
                            </div> 
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <span>INVOICE DATE: 2073/02/23 BS </span>
                            </div>    
                        </div>
                        <div class="row mg10">
                            <div class="col-md-4">
                                <span>ADDRESS   : KUMARIPATI, LALITPUR</span>
                            </div> 
                            <div class="col-md-4">
                                <span>CONTACT NO: 9843232323</span>
                            </div>
                            <div class="col-md-4">
                                <span>(2016/05/31 AD)</span>
                            </div>    
                        </div>
                        <div class="row mgl20 ">
                            <table>
                                <thead cellpadding="20">
                                    <tr>
                                        <th width="50px">SN</th>
                                        <th width="700px">PARTICULARS</th>
                                        <th width="70px">RATE</th>
                                        <th width="70px">QTY</th>
                                        <th>TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody class="mg10" cellpadding="10">
                                    <tr>
                                        <td>1.</td>
                                        <td>SOMETHING CHARGE</td>
                                        <td>115.00</td>
                                        <td>1</td>
                                        <td>115.00</td>  
                                    </tr>
                                    <tr>
                                        <td>1.</td>
                                        <td>SOMETHING CHARGE</td>
                                        <td>115.00</td>
                                        <td>1</td>
                                        <td>115.00</td>  
                                    </tr>
                                    <tr>
                                        <td>1.</td>
                                        <td>SOMETHING CHARGE</td>
                                        <td>115.00</td>
                                        <td>1</td>
                                        <td>115.00</td>  
                                    </tr>
                                </tbody>
                            </table>   
                        </div>
                        <div class="row" style="margin-left: 5px;">
                            <div class="col-md-6">
                                <span>IN WORDS : ONE HUNDERED AND FIFTEEN ONLY</span>
                            </div>
                            <div class="col-md-2">
                               
                            </div>
                            
                            <div class="col-md-2">
                                <span>SUB TOTAL :</span>
                            </div>
                            <div class="col-md-1">
                                <span style=" margin-left: -44px;">115.00</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row" style="margin-left: 5px;">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-2">
                                <span>DISCOUNT :</span>
                            </div>
                            <div class="col-md-1">
                                <span style=" margin-left: -44px;">0</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row" style="margin-left: 5px;">
                            <div class="col-md-6">
                                <span>PRINT DATE/TIME : 2034/34/34- 12:12:12</span>
                            </div>
                            <div class="col-md-2">
                               
                            </div>
                            
                            <div class="col-md-2">
                                <span>TOTAL AMOUNT :</span>
                            </div>
                            <div class="col-md-1">
                                <span style=" margin-left: -44px;">1123.00</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row" style="margin-left: 5px;">
                            <div class="col-md-6">
                                <span>USER : USER NAME</span>
                            </div>
                            <div class="col-md-2">
                               
                            </div>
                            
                            <div class="col-md-2">
                                <span>GRAND TOTAL :</span>
                            </div>
                            <div class="col-md-1">
                                <span style=" margin-left: -44px;">115.00</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row" style="margin-left: 5px;">
                            <div class="col-md-6">
                                <span>DOCTOR : DOCTOR NAME</span>
                            </div>
                            <div class="col-md-2">
                               
                            </div>
                            
                            <div class="col-md-2">
                                <span></span>
                            </div>
                            <div class="col-md-1">
                                <span style=" margin-left: -44px;"></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <span style="margin-top: 20px;margin-bottom: 50px;">GET WELL SOON !!!!</span>
            </div>
        </div>
    </div>
@endsection
@section('footerscript')

@endsection