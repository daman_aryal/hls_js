import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'


Vue.use(VueRouter)

Vue.use(VueResource)


/* eslint-disable no-new */

var router = new VueRouter({
    history: true,
    root: 'super/admin'
})



router.map({
    '/': {
        component: require('./components/Home.vue')
    },
    '/roles/': {
        name: 'roles',
        component: require('./components/Role.vue')
    },
    '/roles/create': {
        name: 'createroles',
        component: require('./components/CreateRole.vue')
    },
    '/roles/:hashid/edit': {
        name: 'editroles',
        component: require('./components/EditRole.vue')
    },
    '/users/': {
        name: 'users',
        component: require('./components/Users.vue')
    },
    '/users/create': {
        name: 'createusers',
        component: require('./components/CreateUser.vue')
    },
    '/users/:hashid/edit': {
        name: 'editusers',
        component: require('./components/EditUser.vue')
    },
    '/patients/': {
        name: 'patient',
        component: require('./components/Patient.vue')
    },
    '/patient/create': {
        name: 'createpatient',
        component: require('./components/CreatePatient.vue')
    },
    '/patient/:hashid/edit': {
        name: 'editpatient',
        component: require('./components/EditPatient.vue')
    },
    '/patient/:hashid': {
        name: 'showpatient',
        component: require('./components/ShowPatient.vue')
    },
    '/posts/': {
        name: 'posts',
        component: require('./components/Posts.vue')
    },
    '/posts/categories/:hashid': {
        name: 'postincats',
        component: require('./components/Posts.vue')
    },
    '/posts/:hashid/edit': {
        name: 'editpost',
        component: require('./components/Editpost.vue')
    },
    '/categories': {
        component: require('./components/Categories.vue')
    },
    '/categories/:hashid/edit': {
        name: 'categories',
        component: require('./components/Editcategory.vue')
    }, 
    '/profile': {
        component: require('./components/Profile.vue')
    },
})

router.alias({

    // alias can contain dynamic segments
    // the dynamic segment names must match
    '/posts/:hashid': '/posts/:hashid/edit',
    'categories/:hashid': '/categories/:hashid/edit'
})

router.start(App, 'body')
